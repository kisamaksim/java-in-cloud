package maksim.ia.authservice;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrincipalController {
    
    @GetMapping("/user")
    public Principal principal(Principal p) {
        return p;
    }
}
