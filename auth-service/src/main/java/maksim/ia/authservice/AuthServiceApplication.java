package maksim.ia.authservice;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

import maksim.ia.authservice.account.Account;
import maksim.ia.authservice.account.AccountRepository;
import maksim.ia.authservice.auth.Client;
import maksim.ia.authservice.auth.ClientRepository;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthorizationServer
public class AuthServiceApplication {
    
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(AuthServiceApplication.class, args);
    
        Client maksim = new Client("maksim", "qwerty");
        Client polly = new Client("polly", "kisa");
        System.out.println(maksim);
        ClientRepository bean = run.getBean(ClientRepository.class);
        bean.saveAll(Arrays.asList(maksim, polly));
        System.out.println("test");
    
    
        Account acc = new Account();
        acc.setActive(true);
        acc.setUsername("kisa");
        acc.setPassword("qwerty");
    
        AccountRepository accountRepository = run.getBean(AccountRepository.class);
        accountRepository.save(acc);
        
    }
    
    
    
}
