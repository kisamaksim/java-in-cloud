package maksim.ia.authservice;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import maksim.ia.authservice.auth.ClientRepository;

@Configuration
public class ClientConfiguration {
    
    @Autowired
    private LoadBalancerClient loadBalancerClient;
    
    @Bean
    ClientDetailsService clientDetailsService(ClientRepository clientRepository) {
        return clientId -> clientRepository
                .findByClientId(clientId)
                .map(
                    client -> {
                        BaseClientDetails details = new BaseClientDetails(client.getClientId(), null,
                                client.getScopes(), client.getAuthorizedGrantTypes(), client.getAuthorities());
                        details.setClientSecret(client.getSecret());
                        
                        String greetingRedirectedUrls = Optional.ofNullable(loadBalancerClient.choose("greeting-client"))
                                .map(serviceInstance -> "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + "/")
                                .orElseThrow(() -> new ClientRegistrationException("couldn't find and bind a greeting-client IP"));
                        details.setRegisteredRedirectUri(Collections.singleton(greetingRedirectedUrls));
                        return details;
                    })
                .orElseThrow(() -> new ClientRegistrationException(String.format("no client %s registered", clientId)));
    }
}
