package maksim.ia.authservice.auth;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    
    Optional<Client> findByClientId(String clientId);
}
