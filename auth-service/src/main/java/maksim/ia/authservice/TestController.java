package maksim.ia.authservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import maksim.ia.authservice.auth.Client;
import maksim.ia.authservice.auth.ClientRepository;

@RestController
@RequestMapping("/test")
public class TestController {
    
    
    @Autowired
    private ClientRepository clientRepository;
    
    @GetMapping("/clients")
    public List<Client> getAllcLientsTest() {
        return clientRepository.findAll();
    }
}
