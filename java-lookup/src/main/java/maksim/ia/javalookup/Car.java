package maksim.ia.javalookup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public class Car {
    
    @Autowired
    private Passenger passenger;
    
    @Lookup
    public Passenger getPassenger() {
        return null;
    }
    
    
    public void printPassenger() {
        System.out.println(passenger);
        System.out.println(getPassenger());
    }
    
}
