package maksim.ia.javalookup;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CaRTwo {
    
    @Autowired
    private Passenger passenger;
    
    @PostConstruct
    public void init() {
        System.out.println("CAR TWO" + passenger);
    }
}
