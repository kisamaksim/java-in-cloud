package maksim.ia.javalookup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JavaLookupApplication {

	public static void main(String[] args) {
		
		ConfigurableApplicationContext run = SpringApplication.run(JavaLookupApplication.class, args);
		Car car = (Car) run.getBean("car");
		
		for (int i = 0; i < 10; i++) {
			car.printPassenger();
		}
		
		String[] activeProfiles = run.getEnvironment().getActiveProfiles();
		String[] defaultProfiles = run.getEnvironment().getDefaultProfiles();
		System.out.println();
	}
	
	@Bean(initMethod = "init")
	TestInitMethods testInitMethods() {
		return new TestInitMethods();
	}

}
