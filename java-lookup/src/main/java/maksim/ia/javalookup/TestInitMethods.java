package maksim.ia.javalookup;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.InitializingBean;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestInitMethods implements InitializingBean {
    
    public void init() {
        log.error("init()");
    }
    
    
    @PostConstruct
    public void postCons() {
        log.error("postCondtract");
    }
    
    
    @Override
    public void afterPropertiesSet() throws Exception {
        log.error("afterPropertiesSet");
    }
    
    @PreDestroy
    public void dest() {
        log.error("destroy");
    }
}
