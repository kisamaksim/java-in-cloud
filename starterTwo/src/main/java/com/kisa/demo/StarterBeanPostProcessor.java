package com.kisa.demo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class StarterBeanPostProcessor implements BeanPostProcessor {
    
    
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (beanName.equals("maksim")) {
            System.out.println("Found in another starter");
        }
        if (beanName.equals("m2mRestTemplate")) {
            System.out.println("Found in another starter");
        }
        return bean;
    }
}
