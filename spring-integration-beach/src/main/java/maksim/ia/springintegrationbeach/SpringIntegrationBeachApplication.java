package maksim.ia.springintegrationbeach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringIntegrationBeachApplication {
    
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SpringIntegrationBeachApplication.class, args);
        Object a = run.getBean("a");
        System.out.println(a);
        RestTempInj bean = run.getBean(RestTempInj.class);
        System.out.println();
    
    }
    
    @Bean
    RestTemplate a() {
        return new RestTemplate();
    }
    
    @Bean
    RestTemplate b() {
        return new RestTemplate();
    }
    
}
