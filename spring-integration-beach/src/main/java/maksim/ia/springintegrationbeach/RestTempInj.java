package maksim.ia.springintegrationbeach;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.Data;

@Component
@Data
public class RestTempInj {
    
    @Autowired
    private RestTemplate beanCUstom;
}
