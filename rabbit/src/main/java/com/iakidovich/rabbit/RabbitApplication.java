package com.iakidovich.rabbit;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
@Data
@EnableDiscoveryClient
@EnableFeignClients
@EnableZuulProxy
public class RabbitApplication {
    
//    @Value("${custom.hi}")
//    private String hi;
    
    public static void main(String[] args) {
    
        ConfigurableApplicationContext run =
                SpringApplication.run(RabbitApplication.class, args);
        RabbitApplication bean = run.getBean(RabbitApplication.class);
//        log.error(bean.getHi());
    
    }
    
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    @Bean
    public CommandLineRunner commandLineRunner(RouteLocator routeLocator) {
        Log log = LogFactory.getLog(getClass());
        return args -> routeLocator.getRoutes().forEach(r -> log.info(String.format("%s (%s) %s", r.getId(),
                r.getLocation(), r.getFullPath())));
    }
    
}
