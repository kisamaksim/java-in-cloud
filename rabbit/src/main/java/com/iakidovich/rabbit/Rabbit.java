package com.iakidovich.rabbit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@RestController
@RefreshScope
@Slf4j
public class Rabbit {
    
//    @Value("${custom.hi}")
//    private String hi;
    
    @Value("${inner.hi}")
    private String innerHi;
    
//    @PostConstruct
//    public void init() {
//      log.error("================================= " + hi);
//    }
//
//    @GetMapping("/hi")
//    public void getHi() {
//      log.error("================================= " + hi);
//    }
    
    @GetMapping("/hihi")
    public void getHihi() {
        log.error("================================= " + innerHi);
    }
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private RabbitClient rabbitClient;
    
    @GetMapping("/test")
    public void test() {
        rabbitClient.hihi();
    }
    
}
