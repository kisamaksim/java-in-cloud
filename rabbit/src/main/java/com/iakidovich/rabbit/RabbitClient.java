package com.iakidovich.rabbit;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(serviceId = "rabbit-service")
public interface RabbitClient {
    
    @GetMapping("/hihi")
    void hihi();
}
