package com.kisamaksim.javaincloud.ch2.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.kisamaksim.javaincloud.ch2.repository.CatRepository;

@RunWith(SpringRunner.class)
public class CatRestControllerTest {
    
    @MockBean
    private CatRepository catRepository;
    
//    @InjectMocks - не работает, наверное нужен мокито ранер
    private CatRestController catRestController;
    
    @Before
    public void setUp() throws Exception {
        catRestController = new CatRestController();
        ReflectionTestUtils.setField(catRestController,"catRepository", catRepository);
    }
    
    @Test
    public void pidar() {
        System.out.println();
    }
}