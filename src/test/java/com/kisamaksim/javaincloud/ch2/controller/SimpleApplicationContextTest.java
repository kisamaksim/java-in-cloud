package com.kisamaksim.javaincloud.ch2.controller;


import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(properties = {"bla.blo=bip"})
@RunWith(SpringRunner.class)
public class SimpleApplicationContextTest {
    
    @Autowired
    private ApplicationContext applicationContext;
    
    @Value("${bla.blo}")
    private String value;
    
    @Test
    public void testApplicationContext() {
        assertNotNull(applicationContext);
        System.out.println(value);
    }
}
