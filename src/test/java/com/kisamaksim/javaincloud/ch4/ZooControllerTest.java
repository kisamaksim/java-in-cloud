package com.kisamaksim.javaincloud.ch4;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ZooController.class)
public class ZooControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Test
    public void getZoo() throws Exception {
        mockMvc.perform(get("/henlo")).andExpect(status().isOk());
    }
}