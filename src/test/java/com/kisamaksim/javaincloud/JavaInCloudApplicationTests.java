package com.kisamaksim.javaincloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.kisamaksim.javaincloud.ch2.entity.Cat;
import com.kisamaksim.javaincloud.ch2.repository.CatRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class JavaInCloudApplicationTests {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private CatRepository catRepository;
    
    @BeforeEach
    public void before() {
        System.out.println("test");
        Stream.of("Felix", "Garfield", "Boris").forEach(name -> catRepository.save(new Cat(name)));
    }

    @Test
    void catsReflectedInRead() throws Exception {
        MediaType mediaType = MediaType.parseMediaType("application/json");
        mockMvc.perform(get("/cats"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(mediaType))
               .andExpect(
                       mvcResult -> {
                           String contentAsString = mvcResult.getResponse().getContentAsString();
                           assertEquals("3", contentAsString.split("totalElements")[1]
                                                                      .split(":")[1].trim()
                                                                      .split(",")[0]);
                       }
               );
    }
    
    @Autowired
    private ApplicationContext applicationContext;
    
    @Test
    void contextLoads() {
        assertNotNull(applicationContext);
    }

}
