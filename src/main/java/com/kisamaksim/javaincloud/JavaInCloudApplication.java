package com.kisamaksim.javaincloud;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;

import com.kisamaksim.javaincloud.ch8.BinarySearch;
import com.kisamaksim.javaincloud.ch8.BinarySearchService;
import com.kisamaksim.javaincloud.filters.AFiltr;
import com.kisamaksim.javaincloud.filters.BFiltr;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Data
@Slf4j
@PropertySource("classpath:custom.properties")
@EnableConfigServer
@EnableEurekaServer
public class JavaInCloudApplication {
    
    @Value("${common.test}")
    private String nameFromCustomProperties;
    
//    @Value("${custom.hi}")
//    private String hii;
    
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(JavaInCloudApplication.class, args);
        JavaInCloudApplication bean = run.getBean(JavaInCloudApplication.class);
        System.out.println();
        
        int[] ar = {1,2,5,56,78,99,156,245,376,420,666};
    
        BinarySearch binarySearch = run.getBean(BinarySearch.class);
        Optional<Integer> integer = binarySearch.binarySearch(ar, 78);
        System.out.println(integer);
    
        BinarySearchService bean1 = run.getBean(BinarySearchService.class);
        BinarySearchService.A a = new BinarySearchService.A();
//        bean1.testA(() -> a.test(););
    }
    @Bean
    public FilterRegistrationBean<AFiltr> aFiltr() {
        FilterRegistrationBean<AFiltr> bFiltrFilterRegistrationBean = new FilterRegistrationBean<>(new AFiltr());
        bFiltrFilterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE);
        
        return bFiltrFilterRegistrationBean;
    }
    
    @Bean
    public FilterRegistrationBean<BFiltr> bFiltr() {
        FilterRegistrationBean<BFiltr> bFiltrFilterRegistrationBean = new FilterRegistrationBean<>(new BFiltr());
        bFiltrFilterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE -1);
        
        return bFiltrFilterRegistrationBean;
    }
    
    
}
