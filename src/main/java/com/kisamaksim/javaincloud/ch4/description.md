# Тестирование
приемы комплексного тестирования концентрируются на написании и выполнении тестов, охватывающих группу модулей 
программного кода, зависящих друг от друга

## Тестирование Spring Boot
тестирование разбивается на два отдельных стиля:
- блочные тесты (unit tests) - тесты, которым не требуется контекст приложения (ApplicationContext)
- комплексные тесты - нужен контекст приложения

@RunWith - сообщает JUnit, какую из стратегий исполнителя тестов применять
@SpringBootTest - показывает, что этот класс является классов тестирования SpringBoot и предоставляет поддержку 
последовательного поиска ContextConfiguration что сообщает классу тестирования, как загружать ApplicationContext
(Если в качестве параметра к аннотации не указаны классы ContextConfiguration, то исходным поведением станет загрузка
контекста приложения ApplicationContext с помощью последовательного поиска аннотации @SpringBootAutoConfiguration в 
классе в корневом каталоге пакета)

## Тестовые срезы
тестовые срезы - выборочная активация по срезам в рамках автоматического конфигурирования отдельно взятых слоев
приложения

## Срезы
@JsonTest - позволяет активировать только ту конфигурацию, которая предназначена для тестирования JSON сериализации
десериализации 

@WebMvcTest - приводит к автоматической конфигкрации инфраструктуры Spring MVC необходимой для тестового взаимодействия
с методами контроллера

MockMvc - клиент для выполнения имитационных запросов к тестируемому контроллеру Spring MVC

@DataJpaTest - для тестирования приложения, используещего дата жпа (поддержка встроенной бд), активируются толькое те
классы, которые требуются для выполнения тестов в отношении хранилищ Spring Data JPA

@RestClientTest - тестирование имеющегося рест темплейта и его взаимодействия с рест-сервисами

## Сквозное тестирование
