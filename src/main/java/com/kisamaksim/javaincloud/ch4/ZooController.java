package com.kisamaksim.javaincloud.ch4;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZooController {
    
    @GetMapping("/henlo")
    public ResponseEntity<Void> getZoo() {
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}
