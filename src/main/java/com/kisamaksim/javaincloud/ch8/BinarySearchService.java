package com.kisamaksim.javaincloud.ch8;

import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.stereotype.Service;

@Service
public class BinarySearchService implements BinarySearch {
    @Override
    public Optional<Integer> binarySearch(int[] array, int searchedValue) {
        int high = array.length - 1;
        int low = 0;
        
        while (low < high) {
            int middle = (high + low) / 2;
            if (array[middle] == searchedValue) {
                return Optional.of(middle);
            }
            
            if (array[middle] < searchedValue) {
                low = middle;
            } else {
                high = middle;
            }
        }
        return Optional.empty();
    }
    
    
    public void testA(Predicate<? super A> bla) {
        return;
    }
    
    public static class A {
        
        public boolean test() {
            return true;
        }
        
    
    }
    
    public static class B extends A {
    
    }
}
