package com.kisamaksim.javaincloud.ch8;

import java.util.Optional;

public interface BinarySearch {
    
    Optional<Integer> binarySearch(int[] array, int searchedValue);
}
