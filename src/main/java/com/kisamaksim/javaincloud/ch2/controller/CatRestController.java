package com.kisamaksim.javaincloud.ch2.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kisamaksim.javaincloud.ch2.entity.Cat;
import com.kisamaksim.javaincloud.ch2.repository.CatRepository;

@RestController
public class CatRestController {
    
    @Autowired
    private CatRepository catRepository;
    
    private List<String> niceWords = Arrays.asList("I <3 U", "Pinsker ты моя любовь)", "bonita<3", "я люблю тебя",
            "ма мун<3", "у тебя все получится)", "murrrr");
    
    @GetMapping("/cats")
    public List<Cat> getCats() {
        return catRepository.findAll();
    }
    
    @PutMapping("/cat/{name}")
    public Cat addCat(@PathVariable String name) {
        return catRepository.save(new Cat(name));
    }
    
    @GetMapping("/love")
    public String love() {
        return "<html> <h1> " +
                niceWords.get(ThreadLocalRandom.current().nextInt(0, niceWords.size())) +
                "</h1> </html>";
    }
    
    @GetMapping("/pidar")
    public String pidar() {
        return "филипп пидармотик, ебаный";
    }
    
}
