package com.kisamaksim.javaincloud.ch2.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "cat")
public class Cat {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @NonNull
    private String name;
}
