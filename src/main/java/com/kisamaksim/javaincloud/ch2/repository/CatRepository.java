package com.kisamaksim.javaincloud.ch2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.kisamaksim.javaincloud.ch2.entity.Cat;

@RepositoryRestResource
public interface CatRepository extends JpaRepository<Cat, Long> {
}
