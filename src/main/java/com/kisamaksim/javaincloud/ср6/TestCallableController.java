package com.kisamaksim.javaincloud.ср6;

import java.util.concurrent.Callable;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/test/callable")
@Slf4j
public class TestCallableController {
    
    @GetMapping
    public Callable<String> testCall() {
        log.error("======before return the result");
        return () -> {
            log.error("=========================== look what a thread");
            return "Hennnnnnnlo";
        };
    }
}
