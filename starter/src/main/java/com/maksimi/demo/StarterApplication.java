package com.maksimi.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class StarterApplication {
    
    @Bean("maksim")
    public ImportedBean bean() {
        return new ImportedBean();
    }
    
    @Bean("m2mRestTemplate")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
