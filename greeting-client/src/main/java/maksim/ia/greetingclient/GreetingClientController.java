package maksim.ia.greetingclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GreetingClientController {
    
    @Autowired
    GreetingClient greetingClient;
    
    @GetMapping("/{name}")
    public ResponseEntity<String> feign(@PathVariable String name){
        return greetingClient.greet(name);
    }
}
