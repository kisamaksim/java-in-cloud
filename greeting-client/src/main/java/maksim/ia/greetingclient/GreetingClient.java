package maksim.ia.greetingclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(serviceId = "greeting-service")
public interface GreetingClient {
    
    @GetMapping("/greet/{name}")
    public ResponseEntity<String> greet(@PathVariable String name);
}
